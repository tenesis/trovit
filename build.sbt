name := "trovit"
scalaVersion in ThisBuild := "2.11.8"
organization in ThisBuild := "com.trovit"
version in ThisBuild := "0.2"

enablePlugins(BuildEnvPlugin)

// Core dependencies
lazy val commonDependencies = Seq(
  //  "org.apache.kafka" %% "kafka" % "1.0.0",
  "org.apache.spark" %% "spark-core" % Versions.spark,
  //  "org.apache.spark" % "spark-streaming_2.11" % Versions.spark,
  //  "org.apache.spark" %% "spark-streaming" % Versions.spark % Provided,
  "org.apache.spark" %% "spark-sql" % Versions.spark,
  "org.apache.spark" %% "spark-streaming-kafka-0-8" % Versions.spark,
  "org.apache.spark" %% "spark-mllib" % Versions.spark,
  "com.typesafe" % "config" % "1.3.0"
)

// Test dependencies
lazy val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % Versions.scalaTest % Test,
  "org.scalamock" %% "scalamock-scalatest-support" % Versions.scalaMock % Test,
  "org.scalacheck" %% "scalacheck" % Versions.scalaCheck % Test,
  "com.holdenkarau" %% "spark-testing-base" % Versions.sparkTestingBase % Test
)

val commonSettings = Seq(
  unmanagedResourceDirectories in Compile += {
    val confFile = buildEnv.value match {
      case BuildEnv.Dev => "dev"
      case BuildEnv.Pre => "pre"
      case BuildEnv.Live => "live"
    }
    (baseDirectory in Compile).value / "conf" / confFile
  },
  libraryDependencies ++= commonDependencies ++ testDependencies
)


lazy val datalakeFeed = (project in file("datalake-feed")).settings(commonSettings)

fork in Test := true

parallelExecution in Test := false

javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")

