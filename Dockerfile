FROM anapsix/alpine-java

ARG kafka_version=1.0.0
ARG scala_version=2.12

RUN apk add --update unzip wget curl docker jq coreutils

ENV KAFKA_VERSION=$kafka_version SCALA_VERSION=$scala_version
ADD ./scripts/download-kafka.sh /tmp/download-kafka.sh
ADD ./scripts/download-kafka.sh /download-kafka.sh

RUN chmod a+x /tmp/download-kafka.sh && sync && /tmp/download-kafka.sh && tar xfz /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz -C /opt && rm /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz && ln -s /opt/kafka_${SCALA_VERSION}-${KAFKA_VERSION} /opt/kafka

VOLUME ["/kafka"]

ENV KAFKA_HOME /opt/kafka

WORKDIR /kafka-feed
ADD ./scripts/emit_random.sh .

ENTRYPOINT /kafka-feed/emit_random.sh | ${KAFKA_HOME}/bin/kafka-console-producer.sh --broker-list kafka:9092 --topic topic