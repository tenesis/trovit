package com.trovit.cars.datalakefeed.boundaries.builders

import com.trovit.cars.datalakefeed.domain.entities.HotelInfo

case class HotelInfoBuilder(hotelCode: Int) {

  //TODO Esto está mal... como mucho es un workaround...
  private var opinions: Option[List[String]] = Some(Nil)
  private var niches: Option[List[String]] = None
  private var city: Option[String] = None
  private var services: Option[List[String]] = None
  private var images: Option[Int] = Some(0)
  private var geo: Option[(String, String)] = None
  private var address: Option[String] = None

  def setGeo(latitude: String, longitude: String): HotelInfoBuilder = {
    this.geo = Some(latitude, longitude)
    this
  }

  def setServices(services: List[String]): HotelInfoBuilder = {
    this.services = Some(services)
    this
  }

  def setNumberOfImages(numberOfImages: Int): HotelInfoBuilder = {
    this.images = Some(numberOfImages)
    this
  }

  def setCity(city: String): HotelInfoBuilder = {
    this.city = Some(city)
    this
  }

  def setAddress(address: String): HotelInfoBuilder = {
    this.address = Some(address)
    this
  }

  def setOpinions(opinions: List[String]): HotelInfoBuilder = {
    this.opinions = Some(opinions)
    this
  }

  def setNiches(niches: List[String]): HotelInfoBuilder = {
    this.niches = Some(niches)
    this
  }

  def createHotelInfo(): HotelInfo = {
    HotelInfo(
      this.hotelCode,
      this.geo.get,
      this.address.get,
      this.images.get,
      this.services.get,
      this.city.get,
      this.niches.get,
      this.opinions.get)
  }

}