package com.trovit.cars.datalakefeed.domain.repository

import org.apache.spark.SparkContext
import org.apache.spark.streaming.StreamingContext

trait KafkaCarsRepository {
  def getDfCars(sparkContext: StreamingContext)
}
