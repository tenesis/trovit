package com.trovit.cars.datalakefeed

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.expressions.Second
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

trait SparkApp extends App {

  private lazy val conf: SparkConf =
    new SparkConf().set("spark.serializer", "org.apache.spark.serializer.KryoSerializer").setAppName("Trovit").setMaster(masterUrl())


  lazy val sparkSession: SparkSession = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()
  lazy val sparkContext: SparkContext = sparkSession.sparkContext

  lazy val streamingContext = new StreamingContext(conf, Seconds(1))

  /*
  lazy val objectMapper: ObjectMapper = {
    val mapper = new ObjectMapper() with ScalaObjectMapper
    mapper.registerModule(DefaultScalaModule)
  }
  */

  private def masterUrl(): String = {
    val defaultMasterUrl = "local[*]"
    if (args == null || args.isEmpty) {
      defaultMasterUrl
    } else {
      Option(args(0)).getOrElse(defaultMasterUrl)
    }
  }

}
