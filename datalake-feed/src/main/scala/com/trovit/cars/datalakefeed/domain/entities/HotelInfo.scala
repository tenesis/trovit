package com.trovit.cars.datalakefeed.domain.entities

case class HotelInfo(hotelCode: Int,
                     geo: (String, String),
                     address: String,
                     images: Int,
                     services: List[String],
                     city: String,
                     niches: List[String],
                     opinions: List[String])
  extends Product {}