package com.trovit.cars.datalakefeed.repository

import com.trovit.cars.datalakefeed.domain.repository.KafkaCarsRepository
import kafka.serializer.{DefaultDecoder, StringDecoder}
import org.apache.log4j.{Level, LogManager}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.StreamingContext

class KafkaCarsRepositoryImp extends KafkaCarsRepository {

  override def getDfCars(sparkContext: StreamingContext): Unit = {
    val log = LogManager.getRootLogger

    val kafkaConf = Map(
      "zookeeper.connect" -> "localhost:2181",
      "group.id" -> "trovit.consumer",
      "zookeeper.connection.timeout.ms" -> "5000"
    )

    val lines = KafkaUtils.createStream[Array[Byte], String, DefaultDecoder, StringDecoder](
      sparkContext, kafkaConf, Map("trovit" -> 1), StorageLevel.MEMORY_ONLY)


    lines.foreachRDD { rdd => {
      val distinct= rdd.distinct()
      print(distinct.toString())
    }
    }
    //    val words = lines.flatMap { case (x, y) => y.split(" ") }
    //    words.print()

  }
}
