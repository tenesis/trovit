package com.trovit.cars.datalakefeed

import com.trovit.cars.datalakefeed.domain.repository.KafkaCarsRepository
import com.trovit.cars.datalakefeed.repository.KafkaCarsRepositoryImp
import org.apache.log4j.LogManager

object TrovitCarsFeedJob extends SparkApp {

  //TODO Dependency injection.
  //------------------- Dependencies -------------------
  val log = LogManager.getRootLogger
  val carsRepository: KafkaCarsRepository = new KafkaCarsRepositoryImp()
  carsRepository.getDfCars(streamingContext)
  streamingContext.start()
  streamingContext.awaitTermination()
  //  val hdfsRepository: HdfsRepository = new HdfsRepositoryImp()
  //  val hotelInfoBoundary = new HotelInfoBoundaryImp()

  //  val hcbsHotelFilesRDD =
  //  carsRepository.getDfCars(sparkContext).persist


  //  val hcbsHotelCompiledPatch =
  //    carsRepository.getDFFromHotelFilesCompiledPatch(sparkContext).persist
  //-----------------------------------------------------

  //  val hcbsHotelInfoWithPatch =
  //    hcbsHotelFilesRDD
  //      .leftOuterJoin(hcbsHotelCompiledPatch)
  //      .map {
  //        case (hotelCode, (jsonFile, jsonPatch)) if jsonPatch.isDefined =>
  //          (hotelCode, applyJsonPatchToArray(jsonFile, jsonPatch.get))
  //        case (hotelCode, (jsonFile, _)) => (hotelCode, jsonFile)
  //      }

  //  val hotelInfoRdd = hcbsHotelInfoWithPatch
  //    .map {
  //      case (hotelCode, hotelInfo) =>
  //        hotelInfoBoundary.getHotelInfoFromJson(hotelCode, hotelInfo)
  //    }

  //  val hotelInfoDF = sparkSession.createDataFrame(hotelInfoRdd)

  log.info("Saving on datalake!")

  //  hdfsRepository.saveHcbsHotelInfo(hotelInfoDF)

  log.info("Finish")

  //  def applyJsonPatchToArray(hotelBase: String, hotelPatch: String): String = {
  //    val patch = JsonPatch.parse(hotelPatch)
  //    patch(hotelBase)
  //  }

}
