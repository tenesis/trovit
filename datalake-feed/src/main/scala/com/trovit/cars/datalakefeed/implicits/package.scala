package com.trovit.cars.datalakefeed

import java.util

import org.bson.Document

import scala.collection.JavaConverters._

package object datalakefeed {

  implicit class ScalaMongoDocument(document: Document) {

    def getList[T](key: String): List[T] = {
      document.get("value").asInstanceOf[util.ArrayList[T]].asScala.toList
    }

  }

}
