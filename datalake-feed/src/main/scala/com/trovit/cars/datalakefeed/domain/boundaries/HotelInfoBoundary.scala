package com.trovit.cars.datalakefeed.domain.boundaries

import com.trovit.cars.datalakefeed.domain.entities.HotelInfo

trait HotelInfoBoundary {
  def getHotelInfoFromJson(hotelCode: Int, hotelJson: String): HotelInfo
}
