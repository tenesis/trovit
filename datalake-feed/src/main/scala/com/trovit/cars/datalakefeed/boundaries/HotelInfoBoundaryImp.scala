package com.trovit.cars.datalakefeed.boundaries

import com.trovit.cars.datalakefeed.boundaries.builders.HotelInfoBuilder
import com.trovit.cars.datalakefeed.domain.boundaries.HotelInfoBoundary
import com.trovit.cars.datalakefeed.domain.entities.HotelInfo
import org.json4s._
import org.json4s.native.JsonMethods._

import scala.util.Try

class HotelInfoBoundaryImp() extends HotelInfoBoundary {
  override def getHotelInfoFromJson(hotelCode: Int,
                                    hotelJson: String): HotelInfo = {
    val hotelInfoBuilder = HotelInfoBuilder(hotelCode)
    val jsonObject = parse(hotelJson)
    val basicInformation = jsonObject \ "BasicInformation"
    val city = (basicInformation \ "Town").values
    val niches =
      Try((basicInformation \ "Niches").values.asInstanceOf[List[String]])
        .getOrElse(List.empty)

    val address = (basicInformation \ "Address").values

    val geoInformation = basicInformation \ "GeoInformation"
    val latitude = (geoInformation \ "Latitude").values
    val longitude = (geoInformation \ "Longitude").values

    val general = jsonObject \ "General"
    val services = Try((general \ "Services").values.asInstanceOf[List[String]])
      .getOrElse(List.empty)

    hotelInfoBuilder
      .setCity(city.toString)
      .setGeo(latitude.toString, longitude.toString)
      .setServices(services)
    hotelInfoBuilder.setAddress(address.toString).setNiches(niches)

    val temp = hotelInfoBuilder.createHotelInfo()
    temp

  }
}
