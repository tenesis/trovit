name := "datalake-feed"

libraryDependencies += "org.mongodb.spark" %% "mongo-spark-connector" % "2.1.0"
libraryDependencies += "org.gnieh" %% "diffson-spray-json" % "2.2.1"
libraryDependencies += "org.json4s" %% "json4s-native" % "3.5.3"